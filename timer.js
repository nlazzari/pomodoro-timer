

var PoModulo = (function(){

	var workColor = "#32fa77",
		breakColor = "#66ccff";

	var interval = null,
		endTime = null;

	var pomodoro = { 
		initTimer: false,
		isTimerON: false,
		isItWorkTime: true,

		workMins: 25,
		breakMins: 5,

	};

	function minsToMs(mins) {
		return Date.parse( new Date() ) + (mins*60*1000);
	}

	
	function getTimeLeft(end) {
		var t = end - Date.parse(new Date());
  		var seconds = Math.floor( (t/1000) % 60 );
		var minutes = Math.floor( (t/1000/60) % 60 );
		var hours = Math.floor( (t/(1000*60*60)) % 24 );
	
		return {
	    	'total': t,
	    	'hours': hours,
	    	'minutes': minutes,
	    	'seconds': seconds
		  };


	}
	
	function playChime() {

		var wav = "http://nlazzari.com/timer/chime.mp3", 
		    audio = new Audio(wav);
			
			audio.play();
	}
	
	function changeColor(selector, color) {
		var sel1 = "#" + selector + "-cluster",
			sel2 = "#" + selector + "-plus",
			sel3 = "#" + selector + "-minus";

		$(sel1).css("color", color);
		$(sel2).css("color", color);
		$(sel3).css("color", color);

		$(".timer-display").css("color", color);
		$("#timer-start-stop").css("color", color);
		$("#timer-start-stop").css("border-color", color);

		
	}

	function updateTimer() {

		$("#hours").text(   ('0' + getTimeLeft(endTime).hours).slice(-2)   );
		$("#minutes").text( ('0' + getTimeLeft(endTime).minutes).slice(-2) );
		$("#seconds").text( ('0' + getTimeLeft(endTime).seconds).slice(-2) );
		
		

		
		  
		//When work time hits zero, load break time to timer, set flag
		if( getTimeLeft(endTime).total <= 0 && pomodoro.isItWorkTime === true) {
	    	playChime();
	    	changeColor("work", "white");
	    	changeColor("break", breakColor);

	    	pomodoro.isItWorkTime = false;
	    	endTime = minsToMs(pomodoro.breakMins);
	    	updateTimer();

		}
		//If break time hits zero, stop time and re-initialize timer
		else if( getTimeLeft(endTime).total <= 0 && pomodoro.isItWorkTime === false ) {
	    	playChime();
	    	pomodoro.timerClick();

	    	changeColor("work", "white");
	    	changeColor("break", "white");

	    	endTime = minsToMs(pomodoro.workMins);
	    	updateTimer();
	    	
	    	pomodoro.initTimer = false;
	    	pomodoro.isItWorkTime = true;

		}

	}



	pomodoro.initTimeDisplay = function() {

		endTime = minsToMs(this.workMins);

		$("#hours").text(   ('0' + getTimeLeft(endTime).hours).slice(-2)   );
		$("#minutes").text( ('0' + getTimeLeft(endTime).minutes).slice(-2) );
		$("#seconds").text( ('0' + getTimeLeft(endTime).seconds).slice(-2) );

		$("#work-minutes").text(  pomodoro.workMins );
		$("#break-minutes").text(  pomodoro.breakMins );
		  
		endTime = null;
	}



	pomodoro.timerClick = function() {

		
		//START timer if timer is currently stopped
		if (this.isTimerON === false) {
			this.isTimerON = true;

			// INITialize the timer with desired duration,
			// if running the timer for the first time
			if (this.initTimer === false) {

				if(this.isItWorkTime) {
					endTime = minsToMs(this.workMins);
					changeColor("work", workColor);
				}
				else {
					endTime = minsToMs(this.breakMins);
					changeColor("break", breakColor);
				}	

				this.initTimer = true;
			} //  /INIT

			else {  //Load the cached time left when the timer is started
				
				//stop the timer : endTime + now
				endTime = endTime +  Date.parse( new Date() );
			}

			updateTimer();
			interval = setInterval(updateTimer, 1000);
		} //  /START

		//STOP timer if currently running
		else
		{
			this.isTimerON = false;
			clearInterval(interval);

			
			// Cache the time left when the timer stopped
			endTime = getTimeLeft(endTime).total;
			
			
		}



	}

	// increments work time by one minute
	pomodoro.workMinPlus = function() {
		// if the timer is off:
		if( !this.isTimerON ) {

			// increment work minutes
			this.workMins++;

			// if it is currently a work session:
			if(this.isItWorkTime ) {	

				// re-initialize and update timer
				endTime = minsToMs(this.workMins);
				this.initTimer = false;
				updateTimer();
			}	

			//update displayed work minutes
			$("#work-minutes").text(  this.workMins );	

		}

	}

	// decrements work time by one minute
	pomodoro.workMinMinus = function() {
		// if the timer is off:
		if( !this.isTimerON ) {

			// decrement work minutes if more than 1 left
			if(this.workMins > 1) {
				this.workMins--;
			}

			// if it is currently a work session:
			if(this.isItWorkTime ) {

				// re-initialize and update timer
				endTime = minsToMs(this.workMins);
				this.initTimer = false;
				updateTimer();
									
			}

			//update displayed work minutes
			$("#work-minutes").text(  this.workMins );	
		}

	}

	// increments break time by one minute
	pomodoro.breakMinPlus = function() {
		// if the timer is off:
		if( !this.isTimerON ) {

			// increment break minutes
			this.breakMins++;

			// if it is currently a break session:
			if(!this.isItWorkTime) {
		
				// re-initialize and update timer	
				endTime = minsToMs(this.breakMins);
				this.initTimer = false;
				updateTimer();
				
			}

			//update displayed break minutes
			$("#break-minutes").text(  this.breakMins );
		}

	}

	// decrements break time by one minute
	pomodoro.breakMinMinus = function() {
		// if the timer is off:
		if( !this.isTimerON ) {

			// decrement break minutes if more than 1 left
			if(this.breakMins > 1) {
				this.breakMins--;
			}

			// if it is currently a break session:
			if(!this.isItWorkTime) {

				// re-initialize and update timer
				endTime = minsToMs(this.breakMins);
				this.initTimer = false;
				updateTimer();	
			}

			//update displayed break minutes
			$("#break-minutes").text(  this.breakMins );
		}

	}


	return pomodoro;

 }());



//console.log( PoModulo.isTimerON );
//PoModulo.timerClick();
//console.log( PoModulo.isTimerON );

//var timeout = setTimeout(PoModulo.timerClick(), 30000);


/*
console.log(PoModulo.m + ":" + PoModulo.s);
PoModulo.inc();
console.log(PoModulo.m + ":" + PoModulo.s);
PoModulo.inc();
console.log(PoModulo.m + ":" + PoModulo.s);
PoModulo.inc();
//*/


