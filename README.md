Live demo: http://nlazzari.com/timer/timer.html

A minimalist productivity timer that uses the Pomodoro Technique. The user may adjust the work and break times separately, and a chime plays upon completion of one Pomodoro cycle. The interface is responsive and adjusts to allow a good experience on mobile, and tablet devices. The minute buttons are disabled while a Pomodoro is active to prevent accidental button presses. 

Technologies used are Javascript, jQuery, HTML/CSS, Skeleton.css, and Font Awesome.